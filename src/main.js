var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var faces = []
var dimension = 31
var initSize = 0.0275
var array = create2DArray(dimension, dimension, 0, true)
var array2 = create2DArray(dimension, dimension, 0, true)

function preload() {
  for (var i = 0; i < 2; i++) {
    faces.push(loadImage('img/fortepan_89003_' + (i * 2) + '.jpg'))
  }
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      array[i][j] = faces[(i + j) % 2].get(Math.floor(faces[0].width / dimension - 1) * i, Math.floor(faces[0].width / dimension - 1) * j, Math.floor(faces[0].width / dimension - 1), Math.floor(faces[0].width / dimension - 1))
    }
  }
  for (var i = 0; i < array2.length; i++) {
    for (var j = 0; j < array2[i].length; j++) {
      array2[i][j] = faces[(i + j + 1) % 2].get((faces[(i + j) % 2].width / dimension) * i, (faces[(i + j) % 2].width / dimension) * j, (faces[(i + j) % 2].width / dimension), (faces[(i + j) % 2].width / dimension))
    }
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize)
      image(array2[i][j],
        -boardSize * initSize * 0.5 * abs(cos(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25)))),
        -boardSize * initSize * 0.5 * abs(cos(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25)))),
        boardSize * initSize * abs(cos(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25)))),
        boardSize * initSize * abs(cos(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25))))
      )
      image(
        array[i][j],
        -boardSize * initSize * 0.5 * abs(sin(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25)))),
        -boardSize * initSize * 0.5 * abs(sin(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25)))),
        boardSize * initSize * abs(sin(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25)))),
        boardSize * initSize * abs(sin(frameCount * 0.05 + ((i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))) * (1 / (dimension * dimension * 0.25))))
      )
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
